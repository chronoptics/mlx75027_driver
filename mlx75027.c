/*
 * MLX75027 Sensor subdev driver
 * Copyright (C) 2019 Chronoptics
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 */

#include "mlx75027.h"

#include <linux/clk.h>
#include <linux/ctype.h>
#include <linux/delay.h>
#include <linux/i2c.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/of_device.h>
#include <linux/of_gpio.h>
#include <linux/pinctrl/consumer.h>
#include <linux/regulator/consumer.h>
#include <linux/slab.h>
#include <linux/types.h>
#include <linux/v4l2-controls.h>
#include <linux/v4l2-mediabus.h>
#include <media/v4l2-ctrls.h>
#include <media/v4l2-device.h>

#define MEDIA_BUS_FMT_RAW12 0x5012

enum mlx75027_mode {
  MLX_CONTINUOUS_INTERNAL,
  MLX_CONTINUOUS_SOFTWARE,
  MLX_TRIGGERED
};

enum mlx75027_output {
  MLX_A_MIN_B = 0b000,
  MLX_A_PLUS_B = 0b001,
  MLX_RAW_A = 0b010,
  MLX_RAW_B = 0b011,
  MLX_RAW_A_AND_B = 0b100
};

enum mlx75027_binning {
  BIN_NO = 0b00,
  BIN_2X2 = 0b01,
  BIN_4X4 = 0b10,
  BIN_8x8 = 0b11
};

enum mlx75027_duty_cycle {
  DUTY_50 = 0b00,
  DUTY_INCREASE = 0b01,
  DUTY_DECREASE = 0b10
};

enum mlx75027_modulation {
  MODULATION = 0b00,
  STATIC_LOW = 0b10,
  STATIC_HI = 0b11
};

struct mlx75027 {
  struct v4l2_subdev subdev;
  struct media_pad pad;
  struct v4l2_captureparm streamcap;
  struct v4l2_pix_format pix;
  struct i2c_client *i2c_client;
  struct mlx75027_parm parms;

  bool on;

  int rst_gpio;
  int pwn_gpio[3];
  u16 hmax;
  u8 data_lanes;
};

struct reg_value {
  u16 u16RegAddr;
  u8 u8Val;
};

// These are the default settings for booting the device.
static struct reg_value mlx75027_init_settings[] = {
    {0x1006, 0x08}, {0x1007, 0x00}, {0x1040, 0x00}, {0x1041, 0x96},
    {0x1042, 0x01}, {0x1043, 0x00}, {0x1044, 0x00}, {0x1046, 0x01},
    {0x104A, 0x01}, {0x1000, 0x00},
};

// Adding 0x1433 results in a write error on register 0x144A
static struct reg_value mlx75027_FULL_init[] = {
    {0x10D2, 0x00}, {0x10D3, 0x10}, {0x1433, 0x00}, {0x1448, 0x06},
    {0x1449, 0x40}, {0x144A, 0x06}, {0x144B, 0x40}, {0x144C, 0x06},
    {0x144D, 0x40}, {0x144E, 0x06}, {0x144F, 0x40}, {0x1450, 0x06},
    {0x1451, 0x40}, {0x1452, 0x06}, {0x1453, 0x40}, {0x1454, 0x06},
    {0x1455, 0x40}, {0x1456, 0x06}, {0x1457, 0x40}, {0x21C4, 0x00},
    {0x2202, 0x00}, {0x2203, 0x1E}, {0x2C08, 0x01}, {0x3C2B, 0x1B},
    {0x400E, 0x01}, {0x400F, 0x81}, {0x40D1, 0x00}, {0x40D2, 0x00},
    {0x40D3, 0x00}, {0x40DB, 0x3F}, {0x40DE, 0x40}, {0x40DF, 0x01},
    {0x412C, 0x00}, {0x4134, 0x04}, {0x4135, 0x04}, {0x4136, 0x04},
    {0x4137, 0x04}, {0x4138, 0x04}, {0x4139, 0x04}, {0x413A, 0x04},
    {0x413B, 0x04}, {0x413C, 0x04}, {0x4146, 0x01}, {0x4147, 0x01},
    {0x4148, 0x01}, {0x4149, 0x01}, {0x414A, 0x01}, {0x414B, 0x01},
    {0x414C, 0x01}, {0x414D, 0x01}, {0x4158, 0x01}, {0x4159, 0x01},
    {0x415A, 0x01}, {0x415B, 0x01}, {0x415C, 0x01}, {0x415D, 0x01},
    {0x415E, 0x01}, {0x415F, 0x01}, {0x4590, 0x00}, {0x4591, 0x2E},
    {0x4684, 0x00}, {0x4685, 0xA0}, {0x4686, 0x00}, {0x4687, 0xA1},
    {0x471E, 0x07}, {0x471F, 0xC9}, {0x473A, 0x07}, {0x473B, 0xC9},
    {0x4770, 0x00}, {0x4771, 0x00}, {0x4772, 0x1F}, {0x4773, 0xFF},
    {0x4778, 0x06}, {0x4779, 0xA4}, {0x477A, 0x07}, {0x477B, 0xAE},
    {0x477D, 0xD6}, {0x4788, 0x06}, {0x4789, 0xA4}, {0x478C, 0x1F},
    {0x478D, 0xFF}, {0x478E, 0x00}, {0x478F, 0x00}, {0x4792, 0x00},
    {0x4793, 0x00}, {0x4796, 0x00}, {0x4797, 0x00}, {0x479A, 0x00},
    {0x479B, 0x00}, {0x479C, 0x1F}, {0x479D, 0xFF}, {0x479E, 0x00},
    {0x479F, 0x00}, {0x47A2, 0x00}, {0x47A3, 0x00}, {0x47A6, 0x00},
    {0x47A7, 0x00}, {0x47AA, 0x00}, {0x47AB, 0x00}, {0x47AC, 0x1F},
    {0x47AD, 0xFF}, {0x47AE, 0x00}, {0x47AF, 0x00}, {0x47B2, 0x00},
    {0x47B3, 0x00}, {0x47B6, 0x00}, {0x47B7, 0x00}, {0x47BA, 0x00},
    {0x47BB, 0x00}, {0x47BC, 0x1F}, {0x47BD, 0xFF}, {0x47BE, 0x00},
    {0x47BF, 0x00}, {0x47C2, 0x00}, {0x47C3, 0x00}, {0x47C6, 0x00},
    {0x47C7, 0x00}, {0x47CA, 0x00}, {0x47CB, 0x00}, {0x4834, 0x00},
    {0x4835, 0xA0}, {0x4836, 0x00}, {0x4837, 0xA1}, {0x4878, 0x00},
    {0x4879, 0xA0}, {0x487A, 0x00}, {0x487B, 0xA1}, {0x48BC, 0x00},
    {0x48BD, 0xA0}, {0x48BE, 0x00}, {0x48BF, 0xA1}, {0x4954, 0x00},
    {0x4955, 0xA0}, {0x4956, 0x00}, {0x4957, 0xA1}, {0x4984, 0x00},
    {0x4985, 0xA0}, {0x4986, 0x00}, {0x4987, 0xA1}, {0x49B8, 0x00},
    {0x49B9, 0x78}, {0x49C2, 0x00}, {0x49C3, 0x3C}, {0x49C8, 0x00},
    {0x49C9, 0x76}, {0x49D2, 0x00}, {0x49D3, 0x3F}, {0x49DC, 0x00},
    {0x49DD, 0xA0}, {0x49DE, 0x00}, {0x49DF, 0xA1}, {0x49EE, 0x00},
    {0x49EF, 0x78}, {0x49F8, 0x00}, {0x49F9, 0x3C}, {0x49FE, 0x00},
    {0x49FF, 0x78}, {0x4A04, 0x00}, {0x4A05, 0x3C}, {0x4A0A, 0x00},
    {0x4A0B, 0x76}, {0x4A10, 0x00}, {0x4A11, 0x3F}, {0x4A1A, 0x00},
    {0x4A1B, 0xA0}, {0x4A1C, 0x00}, {0x4A1D, 0xA1}, {0x4A1E, 0x00},
    {0x4A1F, 0x78}, {0x4A28, 0x00}, {0x4A29, 0x3C}, {0x4A4A, 0x00},
    {0x4A4B, 0xA0}, {0x4A4C, 0x00}, {0x4A4D, 0xA1}, {0x4A7A, 0x00},
    {0x4A7B, 0xA0}, {0x4A7C, 0x00}, {0x4A7D, 0xA1}, {0x4AEE, 0x00},
    {0x4AEF, 0xA0}, {0x4AF0, 0x00}, {0x4AF1, 0xA1}, {0x4B2E, 0x00},
    {0x4B2F, 0xA0}, {0x4B30, 0x00}, {0x4B31, 0xA1}, {0x4B5A, 0x00},
    {0x4B5B, 0xA0}, {0x4B5C, 0x00}, {0x4B5D, 0xA1}, {0x4B86, 0x00},
    {0x4B87, 0xA0}, {0x4B88, 0x00}, {0x4B89, 0xA1}, {0x4B9E, 0x00},
    {0x4B9F, 0x1A}, {0x4BAE, 0x00}, {0x4BAF, 0x1A}, {0x4BB6, 0x00},
    {0x4BB7, 0x1A}, {0x4BC6, 0x00}, {0x4BC7, 0x1A}, {0x4BCE, 0x00},
    {0x4BCF, 0x1A}, {0x4BEE, 0x00}, {0x4BEF, 0xA0}, {0x4BF0, 0x00},
    {0x4BF1, 0xA1}, {0x4BF6, 0x00}, {0x4BF7, 0x1A}, {0x4C00, 0x00},
    {0x4C01, 0x1A}, {0x4C58, 0x00}, {0x4C59, 0xA0}, {0x4C5A, 0x00},
    {0x4C5B, 0xA1}, {0x4C6E, 0x00}, {0x4C6F, 0xA0}, {0x4C70, 0x00},
    {0x4C71, 0xA1}, {0x4C7A, 0x01}, {0x4C7B, 0x35}, {0x4CF2, 0x07},
    {0x4CF3, 0xC9}, {0x4CF8, 0x06}, {0x4CF9, 0x9B}, {0x4CFA, 0x07},
    {0x4CFB, 0xAE}, {0x4CFE, 0x07}, {0x4CFF, 0xC9}, {0x4D04, 0x06},
    {0x4D05, 0x98}, {0x4D06, 0x07}, {0x4D07, 0xB1}, {0x4D18, 0x06},
    {0x4D19, 0xA4}, {0x4D1A, 0x07}, {0x4D1B, 0x49}, {0x4D1E, 0x07},
    {0x4D1F, 0xC9}, {0x4D2A, 0x07}, {0x4D2B, 0xC9}, {0x4D4A, 0x07},
    {0x4D4B, 0xC9}, {0x4D50, 0x06}, {0x4D51, 0x9B}, {0x4D52, 0x07},
    {0x4D53, 0xAE}, {0x4D56, 0x07}, {0x4D57, 0xC9}, {0x4D5C, 0x06},
    {0x4D5D, 0x98}, {0x4D5E, 0x07}, {0x4D5F, 0xB1}, {0x4D70, 0x06},
    {0x4D71, 0xA4}, {0x4D72, 0x07}, {0x4D73, 0x49}, {0x4D78, 0x06},
    {0x4D79, 0xA4}, {0x4D7A, 0x07}, {0x4D7B, 0xAE}, {0x4D7C, 0x1F},
    {0x4D7D, 0xFF}, {0x4D7E, 0x1F}, {0x4D7F, 0xFF}, {0x4D80, 0x06},
    {0x4D81, 0xA4}, {0x4D82, 0x07}, {0x4D83, 0xAE}, {0x4D84, 0x1F},
    {0x4D85, 0xFF}, {0x4D86, 0x1F}, {0x4D87, 0xFF}, {0x4E39, 0x07},
    {0x4E7B, 0x64}, {0x4E8E, 0x0E}, {0x4E9A, 0x00}, {0x4E9C, 0x01},
    {0x4EA0, 0x01}, {0x4EA1, 0x03}, {0x4EA5, 0x00}, {0x4EA7, 0x00},
    {0x4F05, 0x04}, {0x4F0D, 0x04}, {0x4F15, 0x04}, {0x4F19, 0x01},
    {0x4F20, 0x01}, {0x4F66, 0x0F}, {0x500F, 0x01}, {0x5224, 0x00},
    {0x5225, 0x2F}, {0x5226, 0x00}, {0x5227, 0x1E}, {0x5230, 0x00},
    {0x5231, 0x19}, {0x5244, 0x00}, {0x5245, 0x07}, {0x5252, 0x07},
    {0x5253, 0x08}, {0x5254, 0x07}, {0x5255, 0xB4}, {0x5271, 0x00},
    {0x5272, 0x04}, {0x5273, 0x2E}, {0x5281, 0x00}, {0x5282, 0x04},
    {0x5283, 0x2E}, {0x5285, 0x00}, {0x5286, 0x00}, {0x5287, 0x5D},
};

struct mlx75027_data_rate {
  u8 data_lanes;
  u16 com_speed;
  u8 reg_values[10];
};

struct mlx75027_data_rate2 {
  u16 com_speed;
  u8 reg_values[7];
};

// Adding the data rates as specified in the v1.0 datasheet messes stuff up
static struct mlx75027_data_rate data_rates[] = {
    {2, 300, {0x02, 0x58, 0x00, 0x00, 0x09, 0x99, 0x4B, 0x02, 0x01, 0x0C}},
    {2, 600, {0x04, 0xB0, 0x00, 0x00, 0x04, 0xCC, 0x4B, 0x02, 0x00, 0x06}},
    {2, 704, {0x05, 0x80, 0x00, 0x00, 0x04, 0x17, 0x58, 0x02, 0x00, 0x06}},
    {2, 800, {0x06, 0x40, 0x00, 0x00, 0x03, 0x99, 0x64, 0x02, 0x00, 0x06}},
    {2, 960, {0x07, 0x80, 0x00, 0x00, 0x03, 0x00, 0x78, 0x02, 0x00, 0x06}},
    {4, 300, {0x04, 0xB0, 0x00, 0x00, 0x09, 0x99, 0x4B, 0x02, 0x01, 0x0C}},
    {4, 600, {0x09, 0x60, 0x00, 0x00, 0x04, 0xCC, 0x4B, 0x02, 0x00, 0x06}},
    {4, 704, {0x0B, 0x00, 0x00, 0x00, 0x04, 0x17, 0x58, 0x02, 0x00, 0x06}},
    {4, 800, {0x0C, 0x80, 0x00, 0x00, 0x03, 0x99, 0x64, 0x02, 0x00, 0x06}},
    {4, 960, {0x0F, 0x00, 0x00, 0x00, 0x03, 0x00, 0x78, 0x00, 0x00, 0x06}},
};

static struct mlx75027_data_rate2 data_rates2[] = {
    {300, {0x00, 0x1C, 0x01, 0x3A, 0x0A, 0x00, 0xC5}},
    {600, {0x00, 0x0F, 0x00, 0x9D, 0x0A, 0x00, 0xC5}},
    {704, {0x00, 0x0D, 0x00, 0x86, 0x0A, 0x00, 0xC5}},
    {800, {0x00, 0x0B, 0x00, 0x75, 0x0A, 0x00, 0xC5}},
    {960, {0x00, 0x0A, 0x00, 0x62, 0x0A, 0x00, 0xC5}},
};

static u16 data_rate_registers[10] = {0x100C, 0x100D, 0x100E, 0x100F, 0x1016,
                                      0x1017, 0x1045, 0x1047, 0x1060, 0x1071};
static u16 data_rate_registers2[7] = {0x10C2, 0x10C3, 0x10C4, 0x10C5,
                                      0x10D0, 0x10D4, 0x10D5};

struct mlx75027_hmax {
  u8 mode;
  u8 data_lanes;
  u16 com_speed;
  u16 hmax;
};

static struct mlx75027_hmax hmax_values[] = {
    {0, 2, 300, 0x0E78}, {0, 2, 600, 0x0750}, {0, 2, 704, 0x0640},
    {0, 2, 800, 0x0584}, {0, 2, 960, 0x049E}, {0, 4, 300, 0x0860},
    {0, 4, 600, 0x0444}, {0, 4, 704, 0x03A8}, {0, 4, 800, 0x033A},
    {0, 4, 960, 0x02B6}, {4, 2, 300, 0x1A80}, {4, 2, 600, 0x0D54},
    {4, 2, 704, 0x0B60}, {4, 2, 800, 0x0A06}, {4, 2, 960, 0x0860},
    {4, 4, 300, 0x0E60}, {4, 4, 600, 0x0744}, {4, 4, 704, 0x0636},
    {4, 4, 800, 0x057A}, {4, 4, 960, 0x0514},
};

static u16 get_hmax(u8 data_lanes, u16 com_speed, u8 output_mode) {
  struct mlx75027_hmax *hmax = NULL;
  u8 i, size;

  if (output_mode != MLX_RAW_A_AND_B) {
    output_mode = MLX_A_MIN_B;
  }

  size = ARRAY_SIZE(hmax_values);

  for (i = 0; i < size; i++) {
    if (hmax_values[i].mode == output_mode &&
        hmax_values[i].data_lanes == data_lanes &&
        hmax_values[i].com_speed == com_speed) {
      hmax = &hmax_values[i];
      break;
    }
  }

  if (hmax == NULL) return 0;

  return hmax->hmax;
}

static s32 mlx75027_read_reg(struct mlx75027 *sensor, u16 reg, u8 *val) {
  struct device *dev = &sensor->i2c_client->dev;
  u8 au8RegBuf[2] = {0};
  u8 u8RdVal = 0;

  au8RegBuf[0] = reg >> 8;
  au8RegBuf[1] = reg & 0xff;

  if (i2c_master_send(sensor->i2c_client, au8RegBuf, 2) != 2) {
    dev_err(dev, "Read reg error: reg=%x\n", reg);
    return -1;
  }

  if (i2c_master_recv(sensor->i2c_client, &u8RdVal, 1) != 1) {
    dev_err(dev, "Read reg error: reg=%x, val=%x\n", reg, u8RdVal);
    return -1;
  }

  *val = u8RdVal;

  return u8RdVal;
}

static s32 mlx75027_write_reg(struct mlx75027 *sensor, u16 reg, u8 val) {
  struct device *dev = &sensor->i2c_client->dev;
  int ret_val;
  int i;
  u8 au8Buf[3] = {0};

  au8Buf[0] = reg >> 8;
  au8Buf[1] = reg & 0xff;
  au8Buf[2] = val;

  for (i = 0; i < 3; i++) {
    ret_val = i2c_master_send(sensor->i2c_client, au8Buf, 3);
    if (ret_val >= 0) return 0;

    dev_err(dev, "Write reg error: reg=%x, val=%x, error %d\n", reg, val,
            ret_val);
  }

  return -1;
}

static s32 mlx75027_write_reg_array(struct mlx75027 *sensor,
                                    struct reg_value *arr, u32 size) {
  s32 i, retval;

  for (i = 0; i < size; i++) {
    retval = mlx75027_write_reg(sensor, arr[i].u16RegAddr, arr[i].u8Val);
    if (retval < 0) return retval;
  }

  return 0;
}

static s32 mlx75027_param_hold(struct mlx75027 *sensor, bool hold) {
  u8 val = (hold ? 0b1 : 0b0);
  return mlx75027_write_reg(sensor, 0x0102, val);
}

static s32 mlx75027_set_output_configuration(struct mlx75027 *sensor,
                                             u8 data_lanes, u16 com_speed) {
  struct mlx75027_data_rate *data_rate = NULL;
  struct mlx75027_data_rate2 *data_rate2 = NULL;
  u8 i, data_lane_config;
  int size = ARRAY_SIZE(data_rates);

  if (sensor->on) return -1;

  for (i = 0; i < size; i++) {
    if (data_rates[i].data_lanes == data_lanes &&
        data_rates[i].com_speed == com_speed) {
      data_rate = &data_rates[i];
      break;
    }
  }

  // Set Table 21: Data Rate Configuration Settings 2
  for (i = 0; i < ARRAY_SIZE(data_rates2); i++) {
    if (data_rates2[i].com_speed == com_speed) {
      data_rate2 = &data_rates2[i];
      break;
    }
  }

  if (!data_rate) return -1;
  if (!data_rate2) return -1;

  data_lane_config = (data_lanes == 2 ? 0b01 : 0b11);

  mlx75027_write_reg(sensor, 0x1010, data_lane_config);

  for (i = 0; i < 10; i++) {
    mlx75027_write_reg(sensor, data_rate_registers[i],
                       data_rate->reg_values[i]);
  }
  for (i = 0; i < 7; i++) {
    mlx75027_write_reg(sensor, data_rate_registers2[i],
                       data_rate2->reg_values[i]);
  }

  mlx75027_write_reg(sensor, 0x1C40, 0x01);

  return 0;
}

static s32 mlx75027_set_stats_en(struct mlx75027 *sensor, bool stats_en) {
  u8 val = (u8)stats_en;
  return mlx75027_write_reg(sensor, 0x1433, val);
}

static s32 mlx75027_set_stats_mode(struct mlx75027 *sensor, bool stats_mode) {
  u8 val = (u8)stats_mode;
  return mlx75027_write_reg(sensor, 0x14BB, val);
}

static s32 mlx75027_set_lower_limit(struct mlx75027 *sensor, u16 *lower_limit) {
  u8 hi, low, n;

  for (n = 0; n < 8; n++) {
    hi = (lower_limit[n] >> 8) & 0xFF;
    low = (lower_limit[n] & 0xFF);

    mlx75027_write_reg(sensor, 0x1434 + n * 2, hi);
    mlx75027_write_reg(sensor, 0x1435 + n * 2, low);
  }

  return 0;
}

static s32 mlx75027_set_upper_limit(struct mlx75027 *sensor, u16 *upper_limit) {
  u8 hi, low, n;

  for (n = 0; n < 8; n++) {
    hi = (upper_limit[n] >> 8) & 0xFF;
    low = (upper_limit[n] & 0xFF);

    mlx75027_write_reg(sensor, 0x1448 + n * 2, hi);
    mlx75027_write_reg(sensor, 0x1449 + n * 2, low);
  }

  return 0;
}

unsigned char combine_mod(unsigned char *ptr) {
  return (ptr[3] << 6) | (ptr[2] << 4) | (ptr[1] << 2) | ptr[0];
}

static s32 mlx75027_set_dmix0_mod(struct mlx75027 *sensor, u8 *dmix0_mod) {
  u8 val[2];
  val[0] = combine_mod(dmix0_mod);
  val[1] = combine_mod(dmix0_mod + 4);

  mlx75027_write_reg(sensor, 0x21A8, val[0]);
  mlx75027_write_reg(sensor, 0x21A9, val[1]);
  return 0;
}

static s32 mlx75027_set_dmix1_mod(struct mlx75027 *sensor, u8 *dmix1_mod) {
  u8 val[2];
  val[0] = combine_mod(dmix1_mod);
  val[1] = combine_mod(dmix1_mod + 4);

  mlx75027_write_reg(sensor, 0x21AC, val[0]);
  mlx75027_write_reg(sensor, 0x21AD, val[1]);
  return 0;
}

static s32 mlx75027_set_led_mod(struct mlx75027 *sensor, u8 *led_mod) {
  u8 val[2];
  val[0] = combine_mod(led_mod);
  val[1] = combine_mod(led_mod + 4);

  mlx75027_write_reg(sensor, 0x21B0, val[0]);
  mlx75027_write_reg(sensor, 0x21B1, val[1]);
  return 0;
}

static s32 mlx75027_set_frame_startup(struct mlx75027 *sensor,
                                      u32 frame_startup, u16 hmax) {
  u32 val;
  u8 val_hi, val_low;

  val = (frame_startup * 120) / hmax;

  val_low = (val & 0xFF);
  val_hi = (val >> 8) & 0xFF;

  mlx75027_write_reg(sensor, 0x21D4, val_hi);
  mlx75027_write_reg(sensor, 0x21D5, val_low);

  return 0;
}

unsigned char combine_8bits(unsigned char *ptr) {
  return (ptr[7] << 7) | (ptr[6] << 6) | (ptr[5] << 5) | (ptr[4] << 4) |
         (ptr[3] << 3) | (ptr[2] << 2) | (ptr[1] << 1) | ptr[0];
}

static s32 mlx75027_set_phase_idle(struct mlx75027 *sensor, u32 *phase_idle,
                                   u16 hmax) {
  u8 val_idle[8];
  u8 n;
  for (n = 0; n < 8; n++) {
    val_idle[n] = (phase_idle[n] * 120) / hmax;
    if (val_idle[n] < 0x05) {
      val_idle[n] = 0x05;
    }
    mlx75027_write_reg(sensor, 0x21C8 + n, val_idle[n]);
  }
  return 0;
}

static s32 mlx75027_set_preheat(struct mlx75027 *sensor, u8 *preheat) {
  u8 val_preheat = combine_8bits(preheat);
  return mlx75027_write_reg(sensor, 0x21C0, val_preheat);
}

static s32 mlx75027_set_premix(struct mlx75027 *sensor, u8 *premix) {
  u8 val_premix = combine_8bits(premix);
  return mlx75027_write_reg(sensor, 0x21C2, val_premix);
}

static s32 mlx75027_set_pretime_us(struct mlx75027 *sensor, u32 pretime_us,
                                   u16 hmax, u8 output) {
  u32 pretime_reg_val;
  u8 pretime_hi, pretime_low;

  // Calculate the pretime register values
  if (pretime_us == 0) {
    // If pretime is 0 we assume that there is no wanted preheat/premix
    pretime_reg_val = roundup(50 * 120 / hmax, 1);
  } else if (output == MLX_RAW_A_AND_B) {
    pretime_reg_val = roundup((pretime_us * 120) / hmax, 1) + 5;
  } else {
    pretime_reg_val = roundup((pretime_us * 120) / hmax, 1) + 9;
  }

  pretime_low = pretime_reg_val & 0xFF;
  pretime_hi = (pretime_reg_val >> 8) & 0x1F;

  mlx75027_write_reg(sensor, 0x4015, pretime_hi);
  mlx75027_write_reg(sensor, 0x4016, pretime_low);

  return 0;
}

static s32 mlx75027_set_leden(struct mlx75027 *sensor, u8 *leden) {
  // We are using u8 because it is easy ...
  u8 val;
  val = combine_8bits(leden);
  return mlx75027_write_reg(sensor, 0x21C4, val);
}

static s32 mlx75027_set_duty_cycle_value(struct mlx75027 *sensor, u8 value) {
  return mlx75027_write_reg(sensor, 0x21B9, value);
}

static s32 mlx75027_set_duty_cycle(struct mlx75027 *sensor, u8 mode) {
  return mlx75027_write_reg(sensor, 0x4E9E, (u8)mode);
}

static s32 mlx75027_set_operation_mode(struct mlx75027 *sensor, u8 mode) {
  static u16 reg_addresses[6] = {0x2020, 0x2100, 0x2F05,
                                 0x2F06, 0x2F07, 0x3071};
  static u8 reg_values_cont[6] = {0x01, 0x08, 0x01, 0x09, 0x7A, 0x00};
  static u8 reg_values_trig[6] = {0x00, 0x00, 0x07, 0x00, 0x00, 0x03};
  u8 i;
  u8 *reg_val;

  if (sensor->on) return -1;
  if (mode > 2) return -1;

  if (mode == MLX_CONTINUOUS_INTERNAL) {
    reg_values_cont[1] = 0x08;
    reg_val = &reg_values_cont[0];
  } else if (mode == MLX_CONTINUOUS_SOFTWARE) {
    reg_values_cont[1] = 0x01;
    reg_val = &reg_values_cont[0];
  } else if (mode == MLX_TRIGGERED) {
    reg_val = &reg_values_trig[0];
  }

  for (i = 0; i < 6; i++) {
    mlx75027_write_reg(sensor, reg_addresses[i], reg_val[i]);
  }

  return 0;
}

static s32 mlx75027_set_output_mode(struct mlx75027 *sensor, u8 output) {
  if (sensor->on) return -1;
  if (output > 4) return -1;
  return mlx75027_write_reg(sensor, 0x0828, output);
}

static s32 mlx75027_set_hmax(struct mlx75027 *sensor, u16 hmax) {
  u8 val;
  u8 pll_setup = (503 * 120) / hmax + 8;
  u16 pix_rst = (50 * 120) / hmax;
  u32 randnm0 = hmax * pix_rst - 1070 - 2098;

  val = hmax >> 8;
  mlx75027_write_reg(sensor, 0x0800, val);
  val = hmax & 0xff;
  mlx75027_write_reg(sensor, 0x0801, val);

  mlx75027_write_reg(sensor, 0x4010, pll_setup);

  val = pix_rst >> 8;
  mlx75027_write_reg(sensor, 0x4015, val);
  val = pix_rst & 0xff;
  mlx75027_write_reg(sensor, 0x4016, val);

  val = randnm0 >> 16;
  mlx75027_write_reg(sensor, 0x5265, val);
  val = randnm0 >> 8;
  mlx75027_write_reg(sensor, 0x5266, val);
  val = randnm0 & 0xff;
  mlx75027_write_reg(sensor, 0x5267, val);

  return 0;
}

static s32 mlx75027_set_mod_freq(struct mlx75027 *sensor, u32 mod_freq) {
  u16 fmod;
  u8 div_sel_pre, div_sel, fmod_low, fmod_high, low_or_high;

  if (mod_freq < 4 || mod_freq > 150) return -1;

  if (mod_freq == 4) {
    div_sel_pre = 0x03;
  } else if (mod_freq <= 9) {
    div_sel_pre = 0x02;
  } else if (mod_freq <= 18 || (mod_freq >= 21 && mod_freq <= 37) ||
             (mod_freq >= 51 && mod_freq <= 74)) {
    div_sel_pre = 0x01;
  } else {
    div_sel_pre = 0x00;
  }

  if (mod_freq >= 51) {
    div_sel = 0x00;
  } else if (mod_freq >= 21) {
    div_sel = 0x01;
  } else {
    div_sel = 0x02;
  }

  // FMOD[10:0] value is calculated as 2^(DIVSELPRE + DIVSEL) * Modulation
  // Frequency
  fmod = (1 << (div_sel_pre + div_sel)) * mod_freq;

  fmod_low = fmod & 0xff;
  fmod_high = fmod >> 8;

  low_or_high = (fmod * 8 >= 500 && fmod * 8 < 900 ? 0x02 : 0x00);

  mlx75027_write_reg(sensor, 0x21BE, div_sel_pre);
  mlx75027_write_reg(sensor, 0x21BF, div_sel);
  mlx75027_write_reg(sensor, 0x1048, fmod_high);
  mlx75027_write_reg(sensor, 0x1049, fmod_low);
  mlx75027_write_reg(sensor, 0x104B, low_or_high);

  return 0;
}

static s32 mlx75027_set_phase_delay(struct mlx75027 *sensor, u8 *phase_delay) {
  mlx75027_write_reg(sensor, 0x201C, phase_delay[0]);
  mlx75027_write_reg(sensor, 0x201D, phase_delay[1]);
  mlx75027_write_reg(sensor, 0x201E, phase_delay[2]);

  return 0;
}

static s32 mlx75027_set_frame_time(struct mlx75027 *sensor, u32 frame_time,
                                   u16 hmax) {
  u8 val[4];
  u32 reg_time = frame_time * 120 / hmax;

  val[0] = reg_time >> 24;
  val[1] = reg_time >> 16;
  val[2] = reg_time >> 8;
  val[3] = reg_time & 0xff;

  mlx75027_write_reg(sensor, 0x2108, val[0]);
  mlx75027_write_reg(sensor, 0x2109, val[1]);
  mlx75027_write_reg(sensor, 0x210A, val[2]);
  mlx75027_write_reg(sensor, 0x210B, val[3]);

  return 0;
}

static s32 mlx75027_set_phase_cnt(struct mlx75027 *sensor, u8 phase_cnt) {
  if (phase_cnt > 8) return -1;
  return mlx75027_write_reg(sensor, 0x21E8, phase_cnt);
}

static s32 mlx75027_set_integration_time(struct mlx75027 *sensor,
                                         u32 *integration_time, u16 hmax) {
  u8 val[4];
  u8 i;
  u32 time;

  for (i = 0; i < 8; i++) {
    time = ((integration_time[i] * 120) / hmax) * hmax;

    val[0] = time >> 24;
    val[1] = time >> 16;
    val[2] = time >> 8;
    val[3] = time & 0xff;

    mlx75027_write_reg(sensor, 0x2120 + i * 4 + 0, val[0]);
    mlx75027_write_reg(sensor, 0x2120 + i * 4 + 1, val[1]);
    mlx75027_write_reg(sensor, 0x2120 + i * 4 + 2, val[2]);
    mlx75027_write_reg(sensor, 0x2120 + i * 4 + 3, val[3]);
  }

  return 0;
}

unsigned char combine_phases(unsigned char *ptr) {
  return ptr[1] << 4 | ptr[0];
}

static s32 mlx75027_set_phase_shifts(struct mlx75027 *sensor, u8 *phase_shift) {
  u8 val[4];

  val[0] = combine_phases(phase_shift);
  val[1] = combine_phases(phase_shift + 2);
  val[2] = combine_phases(phase_shift + 4);
  val[3] = combine_phases(phase_shift + 6);

  mlx75027_write_reg(sensor, 0x21B4, val[0]);
  mlx75027_write_reg(sensor, 0x21B5, val[1]);
  mlx75027_write_reg(sensor, 0x21B6, val[2]);
  mlx75027_write_reg(sensor, 0x21B7, val[3]);

  return 0;
}

// Disabling test pattern doesn't look to be working
static s32 mlx75027_set_test(struct mlx75027 *sensor, u8 en) {
  if (en) {
    dev_notice(&sensor->i2c_client->dev, "%s(): Enabling test_pattern",
               __func__);

    mlx75027_write_reg(sensor, 0x1405, 0x00);
    mlx75027_write_reg(sensor, 0x1406, 0x04);
    mlx75027_write_reg(sensor, 0x1407, 0x01);
  } else {
    dev_notice(&sensor->i2c_client->dev, "%s(): Disabling test_pattern",
               __func__);

    mlx75027_write_reg(sensor, 0x1407, 0x00);
  }
  return 0;
}

static s32 mlx75027_set_binning(struct mlx75027 *sensor, u8 binning) {
  if (binning > 3) return -1;
  return mlx75027_write_reg(sensor, 0x14A5, binning);
}

static s32 mlx75027_set_roi(struct mlx75027 *sensor, u16 *roi) {
  u8 i, high, low;

  for (i = 0; i < 4; i++) {
    high = roi[i] >> 8;
    low = roi[i] & 0xff;

    mlx75027_write_reg(sensor, 0x0804 + i * 2, high);
    mlx75027_write_reg(sensor, 0x0804 + i * 2 + 1, low);
  }

  return 0;
}

static s32 mlx75027_set_flip(struct mlx75027 *sensor, u8 flip) {
  return mlx75027_write_reg(sensor, 0x080C, flip);
}

static s32 mlx75027_set_mirror(struct mlx75027 *sensor, u8 mirror) {
  return mlx75027_write_reg(sensor, 0x080D, mirror);
}

static s32 mlx75027_set_meta_data(struct mlx75027 *sensor, u8 meta_data) {
  return mlx75027_write_reg(sensor, 0x3C18, meta_data);
}

static s32 mlx75027_set_lvds(struct mlx75027 *sensor, bool lvds) {
  u8 val = (lvds ? 0b1 : 0b0);
  return mlx75027_write_reg(sensor, 0x10E2, val);
}

static s32 mlx75027_set_user_id(struct mlx75027 *sensor, u8 user_id) {
  return mlx75027_write_reg(sensor, 0x0824, user_id);
}

static s32 mlx75027_set_stream(struct mlx75027 *sensor, u8 enable) {
  return mlx75027_write_reg(sensor, 0x1001, enable);
}

static s32 mlx75027_apply_default_parm(struct mlx75027 *sensor);
static s32 mlx75027_check_i2c(struct mlx75027 *sensor);

static s32 mlx75027_reset(struct mlx75027 *sensor) {
  s32 retval = 0;

  // Put the sensor into reset
  gpio_set_value(sensor->rst_gpio, 0);
  // Make sure the sensor has fully been reset
  msleep(5);

  // Take the camera out of reset
  gpio_set_value(sensor->rst_gpio, 1);
  // The sensor has booted.
  msleep(5);

  retval = mlx75027_write_reg_array(sensor, mlx75027_init_settings,
                                    ARRAY_SIZE(mlx75027_init_settings));
  if (retval < 0) return retval;

  // Time between STANDBY OFF and STREAM ON, T7, minimum of 12ms, going with 24m
  // to be safe
  msleep(24);

  retval = mlx75027_write_reg_array(sensor, mlx75027_FULL_init,
                                    ARRAY_SIZE(mlx75027_FULL_init));

  if (retval < 0) return retval;

  retval = mlx75027_apply_default_parm(sensor);
  if (retval < 0) return retval;

  retval = mlx75027_check_i2c(sensor);

  return retval;
}

static s32 mlx75027_set_com_speed(struct mlx75027 *sensor, int com_speed) {
  // Log this because we want to know
  dev_notice(&sensor->i2c_client->dev, "%s(): com_speed %d\n", __func__,
             com_speed);

  // Check com_speed is possible.
  if ((com_speed != 300) && (com_speed != 600) && (com_speed != 704) &&
      (com_speed != 800) & (com_speed != 960)) {
    return -1;
  }

  if (com_speed == sensor->parms.com_speed) {
    // Only update if different.
    return 0;
  }

  // Now set the com_speed in the internal structure
  sensor->parms.com_speed = com_speed;

  // Update all the parameters that depend on com_speed.
  sensor->hmax = get_hmax(sensor->data_lanes, sensor->parms.com_speed,
                          sensor->parms.output);
  mlx75027_set_output_configuration(sensor, sensor->data_lanes,
                                    sensor->parms.com_speed);
  mlx75027_set_hmax(sensor, sensor->hmax);
  mlx75027_set_frame_time(sensor, sensor->parms.frame_time, sensor->hmax);
  mlx75027_set_integration_time(sensor, sensor->parms.integration_time,
                                sensor->hmax);
  mlx75027_set_pretime_us(sensor, sensor->parms.pretime_us, sensor->hmax,
                          sensor->parms.output);
  mlx75027_set_frame_startup(sensor, sensor->parms.frame_startup, sensor->hmax);
  mlx75027_set_phase_idle(sensor, sensor->parms.phase_idle, sensor->hmax);

  return 0;
}

static s32 mlx75027_check_i2c(struct mlx75027 *sensor) {
  s32 ret;
  u8 val;
  struct device *dev = &sensor->i2c_client->dev;
  const u16 user_id_reg = 0x0824;

  ret = mlx75027_read_reg(sensor, user_id_reg, &val);
  if (ret < 0) return ret;
  if (val != 0) {
    dev_err(dev, "mlx75027_check_i2c() failed, expected user id 0 actual =%d\n",
            val);
  }

  val = 1;
  ret = mlx75027_write_reg(sensor, user_id_reg, val);
  if (ret < 0) return ret;

  ret = mlx75027_read_reg(sensor, user_id_reg, &val);
  if (ret < 0) return ret;

  if (val == 1) {
    return 0;
  } else {
    dev_err(dev, "mlx75027_check_i2c() failed, expected user id 1 actual =%d\n",
            val);
    return -1;
  }
}

static s32 mlx75027_apply_default_parm(struct mlx75027 *sensor) {
  struct mlx75027_parm def_parm = {
      .com_speed = 600,

      .integration_time = {500, 500, 500, 500, 250, 250, 250, 250},
      .mod_freq = 50,
      .frame_time = 40000,

      .roi = {1, 640, 0, 241},

      .output = MLX_A_MIN_B,
      .binning = 0,
      .phase_cnt = 4,
      .phase_shifts = {0, 0b100, 0b010, 0b110},

      .pretime_us = 0,
      .preheat = {0, 0, 0, 0, 0, 0, 0, 0},
      .premix = {0, 0, 0, 0, 0, 0, 0, 0},

      .dmix0_mod = {0, 0, 0, 0, 0, 0, 0, 0},
      .dmix1_mod = {0, 0, 0, 0, 0, 0, 0, 0},
      .led_mod = {0, 0, 0, 0, 0, 0, 0, 0},
      .leden = {1, 1, 1, 1, 0, 0, 0, 0},

      .frame_startup = 0,

      .phase_idle = {0, 0, 0, 0, 0, 0, 0, 0},

      .flip = false,
      .mirror = false,
      .test = false,
      .meta_data = false,

      .stats_en = false,
      .stats_mode = false,
      .lower_limit = {0, 0, 0, 0, 0, 0, 0, 0},
      .upper_limit = {0, 0, 0, 0, 0, 0, 0, 0},
      .duty_cycle = 0,
      .duty_cycle_value = 0,
      .phase_delay = {0, 0, 0}};

  sensor->parms = def_parm;

  // XXX : Ideally this is read from the device tree blob.
  sensor->data_lanes = 4;

  sensor->hmax =
      get_hmax(sensor->data_lanes, sensor->parms.com_speed, def_parm.output);

  mlx75027_set_output_configuration(sensor, sensor->data_lanes,
                                    sensor->parms.com_speed);
  mlx75027_set_operation_mode(sensor, MLX_TRIGGERED);
  mlx75027_set_output_mode(sensor, def_parm.output);
  mlx75027_set_hmax(sensor, sensor->hmax);
  mlx75027_set_mod_freq(sensor, def_parm.mod_freq);
  mlx75027_set_frame_time(sensor, def_parm.frame_time, sensor->hmax);
  mlx75027_set_phase_cnt(sensor, def_parm.phase_cnt);
  mlx75027_set_integration_time(sensor, def_parm.integration_time,
                                sensor->hmax);
  mlx75027_set_phase_shifts(sensor, def_parm.phase_shifts);

  mlx75027_set_pretime_us(sensor, def_parm.pretime_us, sensor->hmax,
                          def_parm.output);
  mlx75027_set_preheat(sensor, def_parm.preheat);
  mlx75027_set_premix(sensor, def_parm.premix);

  mlx75027_set_dmix0_mod(sensor, def_parm.dmix0_mod);
  mlx75027_set_dmix1_mod(sensor, def_parm.dmix1_mod);
  mlx75027_set_led_mod(sensor, def_parm.led_mod);
  mlx75027_set_leden(sensor, def_parm.leden);

  mlx75027_set_frame_startup(sensor, def_parm.frame_startup, sensor->hmax);
  mlx75027_set_phase_idle(sensor, def_parm.phase_idle, sensor->hmax);

  mlx75027_set_stats_en(sensor, def_parm.stats_en);
  mlx75027_set_stats_mode(sensor, def_parm.stats_mode);
  mlx75027_set_lower_limit(sensor, def_parm.lower_limit);
  mlx75027_set_upper_limit(sensor, def_parm.upper_limit);
  mlx75027_set_duty_cycle(sensor, def_parm.duty_cycle);
  mlx75027_set_duty_cycle_value(sensor, def_parm.duty_cycle_value);
  mlx75027_set_phase_delay(sensor, def_parm.phase_delay);

  mlx75027_set_binning(sensor, def_parm.binning);
  mlx75027_set_roi(sensor, def_parm.roi);
  mlx75027_set_flip(sensor, def_parm.flip);
  mlx75027_set_mirror(sensor, def_parm.mirror);
  mlx75027_set_meta_data(sensor, def_parm.meta_data);
  mlx75027_set_test(sensor, def_parm.test);
  mlx75027_set_stream(sensor, 0);

  // We use the subLVDS for the illumination modulation.
  // mlx75027_set_lvds(sensor, true);
  // mlx75027_set_user_id(sensor, 0xC3);

  return 0;
}

// See
// https://01.org/linuxgraphics/gfx-docs/drm/media/kapi/v4l2-subdev.html#i2c-sub-device-drivers
static inline struct mlx75027 *to_mlx75027(struct v4l2_subdev *subdev) {
  return container_of(subdev, struct mlx75027, subdev);
}

#ifdef CONFIG_VIDEO_ADV_DEBUG
static int mlx75027_get_register(struct v4l2_subdev *sd,
                                 struct v4l2_dbg_register *reg) {
  struct mlx75027 *sensor = to_mlx75027(sd);
  int ret;
  u8 val;

  reg->size = 1;

  ret = mlx75027_read_reg(sensor, reg->reg, &val);
  if (!ret) reg->val = (__u64)val;

  return ret;
}

static int mlx75027_set_register(struct v4l2_subdev *sd,
                                 const struct v4l2_dbg_register *reg) {
  struct mlx75027 *sensor = to_mlx75027(sd);
  return mlx75027_write_reg(sensor, reg->reg, reg->val);
}
#endif

static struct v4l2_subdev_core_ops mlx75027_subdev_core_ops = {
#ifdef CONFIG_VIDEO_ADV_DEBUG
    .g_register = mlx75027_get_register,
    .s_register = mlx75027_set_register,
#endif
};

static int mlx75027_g_parm(struct v4l2_subdev *sd, struct v4l2_streamparm *a) {
  struct mlx75027 *sensor = to_mlx75027(sd);
  struct device *dev = &sensor->i2c_client->dev;
  struct mlx75027_parm *parm;
  struct v4l2_captureparm *cparm = &a->parm.capture;

  if (a->type != V4L2_BUF_TYPE_VIDEO_CAPTURE) {
    dev_warn(dev, "Unsupported type - %d\n", a->type);
    return -EINVAL;
  }

  memset(a, 0, sizeof(*a));
  a->type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  cparm->capability = sensor->streamcap.capability;
  cparm->timeperframe = sensor->streamcap.timeperframe;
  cparm->capturemode = sensor->streamcap.capturemode;

  // Cast raw data to our parameter struct
  parm = (void *)&a->parm.raw_data;

  memcpy(parm, &sensor->parms, sizeof(sensor->parms));

  return 0;
}

#define CHECK_PARM(NAME, ...)                                        \
  if (parm->NAME != sensor->parms.NAME) {                            \
    retval = mlx75027_set_##NAME(sensor, parm->NAME, ##__VA_ARGS__); \
    if (retval < 0) return retval;                                   \
    sensor->parms.NAME = parm->NAME;                                 \
  }

#define CHECK_PARM_ARRAY(NAME, ...)                                    \
  for (i = 0; i < ARRAY_SIZE(parm->NAME); i++) {                       \
    if (parm->NAME[i] != sensor->parms.NAME[i]) {                      \
      retval = mlx75027_set_##NAME(sensor, parm->NAME, ##__VA_ARGS__); \
      if (retval < 0) return retval;                                   \
      memcpy(sensor->parms.NAME, parm->NAME, sizeof(parm->NAME));      \
      break;                                                           \
    }                                                                  \
  }

static int mlx75027_s_parm(struct v4l2_subdev *sd, struct v4l2_streamparm *a) {
  struct mlx75027 *sensor = to_mlx75027(sd);
  struct device *dev = &sensor->i2c_client->dev;
  struct mlx75027_parm *parm;
  int i, retval;

  if (a->type != V4L2_BUF_TYPE_VIDEO_CAPTURE) {
    dev_warn(dev, "Unsupported type - %d\n", a->type);
    return -EINVAL;
  }

  parm = (void *)&a->parm.raw_data;

  // Reset sensor if communication speed is changed or test mode is changed from
  // on to off
  if (parm->com_speed != sensor->parms.com_speed ||
      parm->test != sensor->parms.test) {
    if (sensor->on) {
      dev_warn(
          dev,
          "Can't change com_speed or test pattern while sensor is running\n");
      return -EINVAL;
    }

    dev_notice(dev, "Resetting sensor");

    retval = mlx75027_reset(sensor);
    if (retval < 0) {
      dev_err(dev, "mlx75027_reset() failed, ret=%d\n", retval);
      return -ENODEV;
    }

    dev_notice(dev, "Sensor reset");
  }

  if (sensor->on) mlx75027_param_hold(sensor, true);

  if (parm->com_speed != sensor->parms.com_speed) {
    mlx75027_set_com_speed(sensor, parm->com_speed);
  }

  CHECK_PARM_ARRAY(integration_time, sensor->hmax)
  CHECK_PARM(mod_freq)
  CHECK_PARM(frame_time, sensor->hmax)

  CHECK_PARM_ARRAY(roi)

  // Check output mode
  if (parm->output != sensor->parms.output) {
    retval = mlx75027_set_output_mode(sensor, parm->output);
    if (retval < 0) return retval;

    sensor->hmax =
        get_hmax(sensor->data_lanes, sensor->parms.com_speed, parm->output);

    retval = mlx75027_set_hmax(sensor, sensor->hmax);
    if (retval < 0) return retval;

    retval = mlx75027_set_integration_time(sensor, parm->integration_time,
                                           sensor->hmax);
    if (retval < 0) return retval;

    retval = mlx75027_set_pretime_us(sensor, parm->pretime_us, sensor->hmax,
                                     parm->output);
    if (retval < 0) return retval;

    sensor->parms.output = parm->output;
  }

  CHECK_PARM(binning)
  CHECK_PARM(phase_cnt)
  CHECK_PARM_ARRAY(phase_shifts)

  CHECK_PARM(pretime_us, sensor->hmax, sensor->parms.output)
  CHECK_PARM_ARRAY(preheat)
  CHECK_PARM_ARRAY(premix)

  CHECK_PARM_ARRAY(dmix0_mod)
  CHECK_PARM_ARRAY(dmix1_mod)
  CHECK_PARM_ARRAY(led_mod)
  CHECK_PARM_ARRAY(leden)

  CHECK_PARM(frame_startup, sensor->hmax)

  CHECK_PARM_ARRAY(phase_idle, sensor->hmax)

  CHECK_PARM(flip)
  CHECK_PARM(mirror)
  CHECK_PARM(test)
  CHECK_PARM(meta_data)

  CHECK_PARM(stats_en)
  CHECK_PARM(stats_mode)
  CHECK_PARM_ARRAY(lower_limit)
  CHECK_PARM_ARRAY(upper_limit)
  CHECK_PARM(duty_cycle)
  CHECK_PARM(duty_cycle_value)

  CHECK_PARM_ARRAY(phase_delay)

  if (sensor->on) mlx75027_param_hold(sensor, false);

  return retval;
}

static int mlx75027_s_stream(struct v4l2_subdev *sd, int enable) {
  struct mlx75027 *sensor = to_mlx75027(sd);
  struct device *dev = &sensor->i2c_client->dev;
  u8 en;

  dev_info(dev, "s_stream: %d\n", enable);

  en = (enable ? 1 : 0);

  sensor->on = en;
  mlx75027_set_stream(sensor, en);

  return 0;
}

static struct v4l2_subdev_video_ops mlx75027_subdev_video_ops = {
    .g_parm = mlx75027_g_parm,
    .s_parm = mlx75027_s_parm,
    .s_stream = mlx75027_s_stream,
};

static int mlx75027_enum_framesizes(struct v4l2_subdev *sd,
                                    struct v4l2_subdev_state *sd_state,
                                    struct v4l2_subdev_frame_size_enum *fse) {
  u32 width, height;
  u32 index = fse->index;

  if (index > BIN_8x8) return -EINVAL;

  width = 640 / (1 << index);
  height = 480 / (1 << index);

  fse->max_width = width;
  fse->min_width = width;
  fse->max_height = height;
  fse->min_height = height;

  return 0;
}

static int mlx75027_enum_frameintervals(
    struct v4l2_subdev *sd, struct v4l2_subdev_state *sd_state,
    struct v4l2_subdev_frame_interval_enum *fie) {
  if (fie->index > 0) return -EINVAL;

  fie->interval.numerator = 1;
  fie->interval.denominator = 500;

  return 0;
}

static int mlx75027_enum_mbus_code(struct v4l2_subdev *sd,
                                   struct v4l2_subdev_state *sd_state,
                                   struct v4l2_subdev_mbus_code_enum *code) {
  if (code->pad || code->index > 0) return -EINVAL;

  code->code = MEDIA_BUS_FMT_RAW12;
  return 0;
}

static int mlx75027_set_fmt(struct v4l2_subdev *sd,
                            struct v4l2_subdev_state *sd_state,
                            struct v4l2_subdev_format *format) {
  struct v4l2_mbus_framefmt *mf = &format->format;
  struct mlx75027 *sensor = to_mlx75027(sd);
  struct device *dev = &sensor->i2c_client->dev;
  int width, height;

  mf->code = MEDIA_BUS_FMT_RAW12;
  mf->colorspace = V4L2_COLORSPACE_RAW;
  mf->field = V4L2_FIELD_NONE;

  if (format->which == V4L2_SUBDEV_FORMAT_TRY) return 0;

  width = sensor->parms.roi[1];
  height = sensor->parms.roi[3] * 2 - sensor->parms.roi[2] * 2 - 2;

  mf->width = width / (1 << sensor->parms.binning);
  mf->height = height / (1 << sensor->parms.binning);

  if (sensor->parms.output == MLX_RAW_A_AND_B) {
    mf->width *= 2;
  }

  dev_notice(dev, "Width %i height %i", mf->width, mf->height);
  return 0;
}

static int mlx75027_get_fmt(struct v4l2_subdev *sd,
                            struct v4l2_subdev_state *sd_state,
                            struct v4l2_subdev_format *format) {
  struct v4l2_mbus_framefmt *mf = &format->format;
  struct mlx75027 *sensor = to_mlx75027(sd);
  int width, height;

  if (format->pad) return -EINVAL;

  mf->code = MEDIA_BUS_FMT_RAW12;
  mf->colorspace = V4L2_COLORSPACE_RAW;
  mf->field = V4L2_FIELD_NONE;

  width = sensor->parms.roi[1];
  height = sensor->parms.roi[3] * 2 - sensor->parms.roi[2] * 2 - 2;

  mf->width = width / (1 << sensor->parms.binning);
  mf->height = height / (1 << sensor->parms.binning);

  return 0;
}

static int mlx75027_camera_link_setup(struct media_entity *entity,
                                      const struct media_pad *local,
                                      const struct media_pad *remote,
                                      u32 flags) {
  return 0;
}

static int mlx75027_validate(const struct v4l2_ctrl *ctrl, u32 idx,
                             union v4l2_ctrl_ptr ptr) {
  return 0;
}

static void mlx75027_init(const struct v4l2_ctrl *ctrl, u32 idx,
                          union v4l2_ctrl_ptr ptr) {
  return;
}

static bool mlx75027_equal(const struct v4l2_ctrl *ctlr, u32 idx,
                           union v4l2_ctrl_ptr prt1, union v4l2_ctrl_ptr ptr2) {
  return 0;
}

static const struct v4l2_ctrl_type_ops mlx75027_ctrl_type_ops = {
    .validate = mlx75027_validate,
    .init = mlx75027_init,
    .equal = mlx75027_equal,
};

static const struct media_entity_operations mlx75027_camera_sd_media_ops = {
    .link_setup = mlx75027_camera_link_setup,
};

static const struct v4l2_subdev_pad_ops mlx75027_subdev_pad_ops = {
    .enum_frame_size = mlx75027_enum_framesizes,
    .enum_frame_interval = mlx75027_enum_frameintervals,
    .enum_mbus_code = mlx75027_enum_mbus_code,
    .set_fmt = mlx75027_set_fmt,
    .get_fmt = mlx75027_get_fmt,
};

static struct v4l2_subdev_ops mlx75027_subdev_ops = {
    .core = &mlx75027_subdev_core_ops,
    .video = &mlx75027_subdev_video_ops,
    .pad = &mlx75027_subdev_pad_ops,
};

static int mlx75027_probe(struct i2c_client *client,
                          const struct i2c_device_id *id) {
  struct pinctrl *pinctrl;
  struct device *dev = &client->dev;
  struct mlx75027 *sensor;
  int retval;
  int i;
  int pwn_gpio_default[] = {GPIOF_OUT_INIT_LOW, GPIOF_OUT_INIT_HIGH,
                            GPIOF_OUT_INIT_HIGH};

  sensor = devm_kzalloc(dev, sizeof(*sensor), GFP_KERNEL);

  pinctrl = devm_pinctrl_get_select_default(dev);
  if (IS_ERR(pinctrl)) dev_warn(dev, "No pin available\n");

  /* request reset pin */
  sensor->rst_gpio = of_get_named_gpio(dev->of_node, "rst-gpios", 0);
  if (!gpio_is_valid(sensor->rst_gpio))
    dev_warn(dev, "No sensor reset pin available");
  else {
    retval = devm_gpio_request_one(dev, sensor->rst_gpio, GPIOF_OUT_INIT_LOW,
                                   "mlx75027_mipi_reset");
    if (retval < 0) {
      dev_warn(dev, "Failed to set reset pin\n");
      return retval;
    }
  }

  // We have powered up VDDMIX, wait T4 100us before we can set RESETB to low
  usleep_range(100, 1000);

  // Enable ToF Power supply
  sensor->pwn_gpio[2] = of_get_named_gpio(dev->of_node, "pwn-gpios", 2);
  if (!gpio_is_valid(sensor->pwn_gpio[2]))
    dev_warn(dev, "No 2 sensor pwdn pin available");
  else {
    retval = devm_gpio_request_one(dev, sensor->pwn_gpio[2],
                                   pwn_gpio_default[2], "mlx75027_power_on");
    if (retval < 0) {
      dev_warn(dev, "Failed to set power pin 2\n");
      dev_warn(dev, "retval=%d\n", retval);
      return retval;
    }
    // We give everything time to warm up
    usleep_range(100, 1000);
  }

  for (i = 0; i < 2; i++) {
    sensor->pwn_gpio[i] = of_get_named_gpio(dev->of_node, "pwn-gpios", i);
    if (!gpio_is_valid(sensor->pwn_gpio[i]))
      dev_warn(dev, "No %i sensor pwdn pin available", i);
    else {
      retval = devm_gpio_request_one(dev, sensor->pwn_gpio[i],
                                     pwn_gpio_default[i], "mlx75027_power_on");
      if (retval < 0) {
        dev_warn(dev, "Failed to set power pin %i\n", i);
        dev_warn(dev, "retval=%d\n", retval);
        return retval;
      }
      // We enable the IO chipset
      usleep_range(100, 1000);
    }
  }

  sensor->i2c_client = client;

  sensor->pix.pixelformat = MEDIA_BUS_FMT_RAW12;
  sensor->pix.width = 640;
  sensor->pix.height = 480;
  sensor->streamcap.capability = V4L2_MODE_HIGHQUALITY | V4L2_CAP_TIMEPERFRAME;
  sensor->streamcap.capturemode = 0;
  sensor->streamcap.timeperframe.denominator = 500;
  sensor->streamcap.timeperframe.numerator = 1;

  retval = mlx75027_reset(sensor);
  if (retval < 0) {
    dev_err(&client->dev, "mlx75027_reset() failed, ret=%d\n", retval);
    return -ENODEV;
  }

  v4l2_i2c_subdev_init(&sensor->subdev, client, &mlx75027_subdev_ops);

  sensor->subdev.flags |= V4L2_SUBDEV_FL_HAS_DEVNODE;
  sensor->pad.flags = MEDIA_PAD_FL_SOURCE;
  sensor->subdev.entity.function = MEDIA_ENT_F_CAM_SENSOR;
  sensor->subdev.entity.ops = &mlx75027_camera_sd_media_ops;
  retval = media_entity_pads_init(&sensor->subdev.entity, 1, &sensor->pad);
  if (retval) {
    return retval;
  }

  retval = v4l2_async_register_subdev(&sensor->subdev);
  if (retval < 0)
    dev_err(&client->dev, "Async register failed, ret=%d\n", retval);

  dev_info(dev, "Camera is found\n");

  return retval;
}

static int mlx75027_remove(struct i2c_client *client) {
  struct v4l2_subdev *sd = i2c_get_clientdata(client);
  struct mlx75027 *sensor = to_mlx75027(sd);

  // Put the sensor into reset
  gpio_set_value(sensor->rst_gpio, 0);

  usleep_range(100, 1000);

  // Stop communication on trigger signal
  gpio_set_value(sensor->pwn_gpio[0], 1);
  // Stop VMIX
  gpio_set_value(sensor->pwn_gpio[1], 0);
  // Stop all ToF related power supplies
  gpio_set_value(sensor->pwn_gpio[2], 0);

  v4l2_async_unregister_subdev(sd);

  media_entity_cleanup(&sensor->subdev.entity);

  return 0;
}

static const struct i2c_device_id mlx75027_id[] = {
    {"mlx75027", 0},
    {},
};
MODULE_DEVICE_TABLE(i2c, mlx75027_id);

static const struct of_device_id mlx75027_camera_dt_ids[] = {
    {.compatible = "mlx,mlx75027"}, {/* sentinel */}};
MODULE_DEVICE_TABLE(of, mlx75027_camera_dt_ids);

static struct i2c_driver mlx75027_i2c_driver = {
    .driver =
        {
            .owner = THIS_MODULE,
            .name = "mlx75027",
            .of_match_table = mlx75027_camera_dt_ids,
        },
    .probe = mlx75027_probe,
    .remove = mlx75027_remove,
    .id_table = mlx75027_id,
};

module_i2c_driver(mlx75027_i2c_driver);

MODULE_AUTHOR(
    "Twan Spil <t.spil@chronoptics.com>, "
    "Refael Whyte <r.whyte@chronoptics.com>, "
    "www.chronoptics.com");
MODULE_DESCRIPTION("MLX75027 Sensor subdev driver");
MODULE_LICENSE("GPL");
