from conan import ConanFile
from conan.tools.files import copy

import os


class ChronopticMLX75027Driver(ConanFile):
    python_requires = "chrono_conan/[>=1.0.0]"
    python_requires_extend = "chrono_conan.ChronoBase"
    remote_name = "mlx75027_driver"

    name = "mlx75027_driver"
    license = "GPL-v2"
    url = "https://www.chronoptics.com"
    description = "Kernel driver for mlx75027"

    def package(self):
        copy(
            self,
            "mlx75027.h",
            self.source_folder,
            os.path.join(self.package_folder, "include"),
        )

    def package_info(self):
        self.cpp_info.includedirs = ["include"]
