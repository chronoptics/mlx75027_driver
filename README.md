To build module:

    export KERNEL_SRC=/lib/modules/$(shell uname -r)/build
    make

To build module with yocto SDK, the first time run:

    cd /<sdk-install-path>/sysroots/<mach>/usr/src/kernel
    sudo bash -c "source <sdk-install-path>/environment-setup-<mach> && make modules_prepare"

And then every time:

    source <sdk-install-path>/environment-setup-<mach>
    export KERNEL_SRC=/<sdk-install-path>/sysroots/<mach>/usr/src/kernel
    make
