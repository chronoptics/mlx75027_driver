#ifndef _MLX_75027_DRIVER_H_
#define _MLX_75027_DRIVER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <linux/types.h>

struct mlx75027_parm {
  __u16 com_speed;

  __u32 integration_time[8];  // in us per phase
  __u32 mod_freq;             // in MHz
  __u32 frame_time;           // in us

  __u16 roi[4];  // col begin, col end, row begin, row end

  __u8 output;
  __u8 binning;
  __u8 phase_cnt;
  __u8 phase_shifts[8];  // i * 45deg i.e. 2 = 90 deg

  __u32 pretime_us;  // The preheat time in us
  __u8 preheat[8];
  __u8 premix[8];

  __u8 dmix0_mod[8];
  __u8 dmix1_mod[8];
  __u8 led_mod[8];
  __u8 leden[8];

  __u32 frame_startup;  // The time between a trigger pulse adn the start of the
                        // first phase acquisition

  __u32 phase_idle[8];  // The idle time after each raw frame in us.

  __u8 flip;
  __u8 mirror;
  __u8 test;
  __u8 meta_data;

  __u8 stats_en;
  __u8 stats_mode;
  __u16 lower_limit[8];  // Unsure if this is 12bit signed or unsigned
  __u16 upper_limit[8];  // Unsure if this is 12bit signed or unsigned
  __u8 duty_cycle;
  __u8 duty_cycle_value;

  __u8 phase_delay[3];  // Coarse, fine, super fine
};

#ifdef __cplusplus
static_assert(sizeof(mlx75027_parm) <= 200);

} /* extern "C" */
#endif

#endif